#!/home/neko/insertelastic_env/bin/python3
import sys, json, gzip, pdb
from parsebrologs import ParseBroLogs
from collections import OrderedDict
import multiprocessing as mp

from elasticsearch.exceptions import NotFoundError
from elasticsearch import Elasticsearch, helpers
import requests, argparse
import time, os, datetime

import hashlib

import glob
from io import StringIO
import csv


import inspect


#PARAMS=""
#URL="http://127.0.0.1:9999/insert/1/prometheus/api/v1/import/csv"
ELASTIC_SEARCH_URL = "127.0.0.1"
PORT = 9999
 
class formater(OrderedDict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.properties=""
   
   
    def get_time(self):
        date = self.get("open")
        date = datetime.datetime.strptime(date[0], "%Y-%m-%d-%H-%M-%S")
        return date.strftime("%Y%m%d")
   
    
    def compose_properties(self):
        types={
            "id.orig_h":"ip",
            "id.resp_h":"ip",
            "ts":"date"
            }
        fields_list = self.get("fields")
        types_list = self.get("types")
        properties=dict()
        #fields_list.pop(len(fields_list)-1)
        for x in fields_list:
            x = x.rstrip()
            if x == "ts":
                properties[x] = { "type": "date", "format" : "yyyy-MM-dd HH:mm:ss"}
                continue
            properties[x] = { "type": types.get(x, "keyword")}
        self.properties = properties     

    def get_properties(self):
        return self.properties


class MyParseBroLogs(ParseBroLogs):
    def __init__(self, filepath, fields=None):
        self.elastic = elastic_search()
        self.filename = filepath
        self.complete = False
        super().__init__(filepath, fields=None)
       
    def _read_log(self, std_buffer, fields=None):
        options = formater()       
        self.filtered_fields = fields
        
        options['data'] = []
        options['separator'] = "\t"  # Set a default separator in case we don't get the separator
        
        with gzip.open(std_buffer, 'rt') as inbuffer:
            lines = inbuffer.read().splitlines()
            count=0
            for line in lines:
                if line.startswith("#separator"):
                    key = str(line[1:].split(" ")[0])
                    value = str.encode(line[1:].split(" ")[1].strip()).decode('unicode_escape')
                    options[key] = value
                    count+=1
                elif line.startswith("#"):
                    key = str(line[1:].split(options.get('separator'))[0])
                    value = line[1:].split(options.get('separator'))[1:]
                    options[key] = value
                    count+=1
                else:
                    break
            name = os.path.split(self.filename)[1].split(".")[0]
            index = name+options.get_time()
            if not self.elastic.if_index(index):
                print("create the index {}".format(self.filename, index))
                options.compose_properties()
                self.elastic.create_index(index, options.get_properties())
            else:
                print("insert file {} to index {}".format(self.filename, index)) 
                self.elastic.insert(index, options["fields"], lines[count:], options['separator'])
                self.complete = True
            #pool = mp.Pool(8)
            #pool.map(self.insert, lines[count:])
            #pool.close()
            #pool.join()
        return options

    def get_status(self):
       return self.complete

class elastic_search():
    def __init__(self):
        insert=False
        self.es = Elasticsearch(hosts=ELASTIC_SEARCH_URL, port=PORT)

    def search(self, _index):
       try:
           result = self.es.search(index=_index, body={"query": {"match_all": {}}})
       except NotFoundError:
           print("There is no index named {}".format(_index))
           #self.create_index(_index, properties)
       #return result["hits"]["hits"]
    
    def create_index(self, _index, properties):
       body = {"settings" : {
                   "number_of_shards" : 1, "number_of_replicas" : 1
               },
               "mappings" : {
                   "properties": properties
               }
              }
       self.es.indices.create(index=_index, body=body)

    def insert(self, _index, field_list, data_list, split_word):
        #for x in self._yield_data(_index, field_list, data_list, split_word):
            #print(json.dumps(x, indent=4))
        helpers.bulk(self.es, self._yield_data(_index, field_list, data_list, split_word))

    def get_index(self, _index):
       return self.es.indices.get(index=_index)

    def if_index(self, _index):
       return self.es.indices.exists(index=_index)

    def delete_index(self, _index): 
       self.es.indices.delete(index=_index, ignore=[400, 404])

    def _yield_data(self, _index, fields, content, split_word):
       
      def _define_field(_fields, _content):
           _dict = dict()
           for d in _content:
               #content=[ 'x y z' ,'x yz z' ,'x yy z' ]
               #d="x y z"
               if not d.startswith("#"):
                   _d = d.split(split_word)
                   if len(_fields) == len(_d): 
                       
                       _dict = dict(zip(_fields, _d))
                       
                       if _dict["uid"] == "-":
                           _id = hashlib.sha3_256(d.encode("utf-8")).hexdigest()
                       else:
                           _id = _dict["uid"]
                       if _dict["id.resp_h"] == '-':
                           _dict["id.resp_h"] = None
                       if _dict["id.orig_h"] == '-':
                           _dict["id.orig_h"] = None
                       _dict["ts"] = datetime.datetime.utcfromtimestamp(float(_dict["ts"])).strftime("%Y-%m-%d %H:%M:%S")
                       yield _id, _dict
        
      for f in _define_field( fields, content):
          yield {
              "_index": _index,
              "_op_type": "index",
              "_id": f[0], #_id
              "_source": f[1] #_dict from above
              } 


def option():
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("-d", "--date", dest="date", action="store", help="The format need to be YYYY-MM-DD. Use wildcard (*) to specify the folder.")
    parser.add_argument("-f", "--file_form", dest="file", action="store", help="Bro data file form, it can be conn, weird, ...etc.")
    args = parser.parse_args() 
    
    return args


def mapper(file):
    return MyParseBroLogs(file).get_status()

if __name__ == "__main__":
    #elastic = elastic_search()
    #elastic.delete_index("weird20201102")
    

    #MyParseBroLogs("/opt/zeek/logs/2021-03-22/*")

    args = option()
    
    args_date = args.date
    args_file = args.file
    path_list = glob.glob('/opt/zeek/logs/'+ "{}".format(args_date))   
 
    #path_list = glob.glob('/data01/bro/logs/2020-11-*')
    for path in path_list:
        file_list = glob.glob(path+'/'+ "{}".format(args_file) +'.*')
        
        pool = mp.Pool(16)

        if not mapper(file_list[0]):
            pool.map(mapper, file_list)
        else:
            pool.map(mapper, file_list[1:])
        #pool.map(mapper, file_list)

        pool.close()
        pool.join()
        

    #pool = mp.Pool(4)
    #pool.map(mapper, file_list)
    #pool.close()
    #pool.join()
